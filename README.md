# prosjekt-oversikt

This CKAN catalog contains a special harvest which reimplements a subset of the CKAN API
in order to create a catalog of NINA projects.

## Setup

1. Create a `.env` file containing `LDAP_PASSWORD=` followed by the NINA LDAP password
2. `docker compose build`
3. `docker compose --profile prod run --rm --service-ports ckan`

### Development

```bash
docker compose --profile dev run --rm -it --service-ports ckan-dev
```

### CKAN Setup

1. [Customize site settings](http://localhost:5000/ckan-admin/config)
   1. Set `Homepage:` to the second or third choice (because of a [CSS glitch](https://github.com/ckan/ckan/issues/6542))
2. Wait ~20 minutes (or more, depending to your settings) to see the harvest in action or run it manually

### Add admin

1. [Login](http://localhost:5000/user/login)
2. Set your user (`$user`) as administrator
   1. `docker compose --profile prod exec ckan ckan -c /etc/ckan/production.ini sysadmin add $user`

## Reset

This command will delete all the containers and all the volumes (`-v`).

``` 
docker compose down -v
```
