#!/bin/bash
set -e

/ckan-entrypoint.sh

conf="/etc/ckan/production.ini"
alias conf_set="crudini --set $conf app:main"
alias conf_get="crudini --get $conf app:main"
alias conf_set_list="crudini --set --list --list-sep=' ' $conf app:main"

shopt -s expand_aliases

#ckan
conf_set ckan.site_title "NINA prosjekt oversikt"
conf_set ckan.site_description "En katalog som inneholder alle prosjektene i NINA"
conf_set ckan.site_logo "https://www.nina.no/Portals/NINA/Bilder/logoweb.png"
conf_set ckan.site_intro_text "NINA prosjekt-oversikt gir mulighet til å søke i alle NINA-prosjeter. De kan søkes opp på mange ulike måter, feks med navn, nøkkelord, avdeling, prosjektnummer eller beskrivelse."
conf_set ckan.site_about "
# About
$(conf_get ckan.site_intro_text)

# Developers
Source code available at [gitlab.com/nina-data/prosjekt-oversikt](https://gitlab.com/nina-data/prosjekt-oversikt).

# Administrators
Harvesting operations can be monitored at [/harvest](/harvest).

# API
In addition to [CKAN API](https://docs.ckan.org/en/2.9/api/) an additional endpoint to export all the datasets as CSV/TSV is provided: [/csv/export](/csv/export).
"

#ckanext-prosjektoversikt_branding
conf_set_list ckan.plugins "prosjektoversikt_branding"

#ckanext-scheming
conf_set_list ckan.plugins "scheming_datasets prosjektoversikt_scheming"
conf_set scheming.dataset_schemas "ckanext.prosjektoversikt_scheming:nina_project.yaml"

#ckanext-harvest
conf_set_list ckan.plugins "harvest ckan_harvester"
conf_set ckan.harvest.mq.type "redis"
conf_set ckan.harvest.mq.hostname "redis"
conf_set ckan.harvest.not_overwrite_fields "notes tags topics"

#ckanext-ldap
conf_set_list ckan.plugins "ldap"
conf_set ckan.auth.create_user_via_web false
conf_set ckanext.ldap.uri "ldap://nindc05.nina.no:389"
conf_set ckanext.ldap.auth.dn "ldapquery"
conf_set ckanext.ldap.base_dn "ou=brukere,dc=nina,dc=no"
conf_set ckanext.ldap.search.filter "sAMAccountName={login}"
conf_set ckanext.ldap.username "sAMAccountName"
conf_set ckanext.ldap.email "mail"
conf_set ckanext.ldap.ckan_fallback "true"
conf_set ckanext.ldap.auth.password "$LDAP_PASSWORD"
conf_set ckanext.ldap.prevent_edits true

#ckanext-prosjektoversikt_auth
conf_set_list ckan.plugins "prosjektoversikt_auth"
conf_set ckan.auth.allow_dataset_collaborators "true"

#ckanext-prosjektoversikt_csv
conf_set_list ckan.plugins "prosjektoversikt_csv"

if [ "$CKAN_EXTRA" = "true" ]
then
  ckan --config=$CKAN_INI harvester initdb
else
  ckan --config=$CKAN_INI search-index rebuild
fi

exec "$@"
