import ckan.plugins.toolkit as toolkit

import csv
import StringIO

fields = (
    "id",
    "title",
    "notes",
    "tags",
    "topics",
    "category",
    "customer",
    "startdate",
    "enddate",
    "budget",
    "project_leader",
    "email",
    "groups",
)


class CSV(toolkit.BaseController):
    def export(self):
        # fetch data
        context = {}
        length = len(toolkit.get_action('package_list')(context, {}))
        rows = 100
        packages = []
        for start in range(0, length, rows):
            packages.extend(toolkit.get_action('package_search')({}, {
                'rows': rows,
                'start': start,
            })['results'])
        # keep specified fieods
        flat = []
        for result in packages:
            partial = {}
            for key, value in result.items():
                if key in 'tags':
                    partial[key] = ','.join(v['name'] for v in value).encode("utf-8")
                elif key == 'topics':
                    partial[key] = ','.join(value).encode("utf-8")
                elif key == 'groups':
                    partial[key] = ' '.join(group['title'] for group in value).encode("utf-8")
                elif key in fields:
                    partial[key] = value.encode("utf-8")
            flat.append(partial)
        # dictionary to csv conversion
        fieldnames = set(key for row in flat for key in row.keys())
        content = StringIO.StringIO()
        writer = csv.DictWriter(content, fieldnames=fields, dialect='excel-tab')
        writer.writeheader()
        for row in flat:
            writer.writerow(row)
        # prepare reply
        toolkit.response.headers['Content-Type'] = 'text/csv; charset=utf-8'
        toolkit.response.headers['Content-Disposition'] = 'attachment; filename="datasets.csv"'
        toolkit.response.headers['Content-Length'] = content.len
        return content.getvalue()
