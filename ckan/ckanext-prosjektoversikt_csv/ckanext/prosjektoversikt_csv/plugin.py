import ckan.plugins as plugins
import ckan.plugins.toolkit as toolkit


class ProsjektoversiktCsvPlugin(plugins.SingletonPlugin):
    plugins.implements(plugins.IConfigurer)
    plugins.implements(plugins.IRoutes, inherit=True)

    controller = "ckanext.prosjektoversikt_csv.controllers:CSV"

    # IConfigurer

    def update_config(self, config_):
        toolkit.add_template_directory(config_, 'templates')
        toolkit.add_public_directory(config_, 'public')
        toolkit.add_resource('fanstatic',
            'prosjektoversikt_csv')

    # IRoutes

    def after_map(self, _map):
        _map.connect(
            '/csv/export',
            controller=self.controller,
            action='export',
        )
        return _map
