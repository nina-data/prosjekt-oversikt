#!/usr/bin/env python3

import polib
import re

replace = {
  re.compile('(?<!{)'+key): value for key, value in {
    "dataset": "project",
    "Dataset": "Project",
    "group": "department",
    "Group": "Department",
  }.items()
}

pofile = polib.pofile('ckan.po')
po = polib.POFile()
for key in ["Language", "Content-Type"]:
    po.metadata[key] = pofile.metadata[key]

for entry in pofile:
    is_plural = False
    if hasattr(entry, 'msgstr_plural'):
        if len(entry.msgstr_plural.keys()) == 2:
          is_plural = True
    for regex, new in replace.items():
        if is_plural:
            if regex.search(entry.msgid):
                entry.msgstr_plural[0] = regex.sub(new, entry.msgid)
            if regex.search(entry.msgid_plural):
                entry.msgstr_plural[1] = regex.sub(new, entry.msgid_plural)
        elif regex.search(entry.msgid):
            entry.msgstr = regex.sub(new, entry.msgid)
    if entry.translated():
        po.append(entry)

po.save('ckan_generated.po')
