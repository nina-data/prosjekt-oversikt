#!/bin/bash
set -euo pipefail

apikey=$(ckan --config=$CKAN_INI user show default |& tail -n1 |
    sed -n 's/.*apikey=\([0-9a-f-]*\).*/\1/p')
curl -X POST -H "Authorization: $apikey" \
    -F "name=nina" \
    -F "title=NINA"\
    http://ckan:5000/api/3/action/organization_create
ckan --config=$CKAN_INI harvester source create \
    prosjektapi http://prosjektapi/ckan ckan projektapi true nina always \
    '{"remote_groups":"create"}'
