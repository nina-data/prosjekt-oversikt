import ckan.plugins as plugins
import ckan.plugins.toolkit as toolkit
import ckanext.prosjektoversikt_scheming.helpers as helpers

from collections import OrderedDict
import json


class ProsjektoversiktSchemingPlugin(plugins.SingletonPlugin):
    plugins.implements(plugins.IConfigurer)
    plugins.implements(plugins.IFacets)
    plugins.implements(plugins.IPackageController, inherit=True)
    plugins.implements(plugins.ITemplateHelpers)

    # IConfigurer

    def update_config(self, config_):
        toolkit.add_template_directory(config_, 'templates')
        toolkit.add_public_directory(config_, 'public')
        toolkit.add_resource('fanstatic',
            'prosjektoversikt_scheming')

    # IFacets

    def dataset_facets(self, facets_dict, package_type):
        facets_dict = OrderedDict()
        facets_dict['vocab_topics'] = toolkit._('Topics')
        facets_dict['tags'] = toolkit._('Tags')
        facets_dict['category'] = toolkit._('Category')
        facets_dict['project_state'] = toolkit._('Status')
        facets_dict['customer'] = toolkit._('Customer')
        facets_dict['groups'] = toolkit._('Department')
        facets_dict['project_leader'] = toolkit._('Project Leader')
        return facets_dict

    def group_facets(self, facets_dict, group_type, package_type):
        del facets_dict['groups']
        return self.dataset_facets(facets_dict, package_type)

    def organization_facets(self, facets_dict, organization_type, package_type):
        return self.dataset_facets(facets_dict, package_type)

    # IPackageController

    def before_index(self, pkg_dict):
        pkg_dict["vocab_topics"] = json.loads(pkg_dict.get("topics", '[]'))
        pkg_dict["project_leader"] = pkg_dict["maintainer"]
        return pkg_dict

    # ITemplateHelpers

    def get_helpers(self):
        return { name:getattr(helpers, name) for name in dir(helpers) }
