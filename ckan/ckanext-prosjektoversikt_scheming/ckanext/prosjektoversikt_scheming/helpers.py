# -*- coding: utf-8 -*-

import os.path
import string

file_dir = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'choices')

translate_table = {
    'Æ': 'AE',
    'Ø': 'OE',
    'Å': 'AA',
    'æ': 'ae',
    'ø': 'oe',
    'å': 'aa',
}

def get_value_from_label(label):
    result = ''
    for character in label.lower():
        if character in string.punctuation or character in string.whitespace:
            result += '-'
        elif character in translate_table.keys():
            result += translate_table[character]
        else:
            result += character
    result = result.encode('ascii', 'ignore').decode('utf-8')
    return result

topics_choices = []
topics_file = os.path.join(file_dir, 'topics.txt')
with open(topics_file, 'r', encoding="utf-8") as choices_topics_file:
    for line in choices_topics_file.readlines():
        label = line.strip()
        topics_choices.append({
            'value': get_value_from_label(label),
            'label': label,
        })

def topics_choices_helper(field):
    return topics_choices
