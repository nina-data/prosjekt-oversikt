import ckan.plugins.toolkit as toolkit
from ckan.logic import chained_action

@chained_action
def user_create(original_action, context, data_dict):
    user = original_action(context, data_dict)
    index = 0
    while True:
        packages = toolkit.get_action('package_search')(context, {
            'start': index,
            'q': 'maintainer_email:'+user['email'],
        })['results']
        for package in packages:
            toolkit.get_action('package_collaborator_create')(context, {
                'id': package['id'],
                'user_id': user['id'],
                'capacity': 'editor',
            })
        index += len(packages)
        if len(packages) == 0:
            break
    return user
