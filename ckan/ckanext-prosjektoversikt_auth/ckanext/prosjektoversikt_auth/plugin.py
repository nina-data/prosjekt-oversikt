import ckan.plugins as plugins
import ckan.plugins.toolkit as toolkit

from ckanext.prosjektoversikt_auth.logic.action.get import user_create
import ckan.model as model

class ProsjektoversiktAuthPlugin(plugins.SingletonPlugin):
    plugins.implements(plugins.IConfigurer)
    plugins.implements(plugins.IActions)
    plugins.implements(plugins.IPackageController, inherit=True)

    # IConfigurer

    def update_config(self, config_):
        toolkit.add_template_directory(config_, 'templates')
        toolkit.add_public_directory(config_, 'public')
        toolkit.add_resource('fanstatic',
            'prosjektoversikt_auth')

    # IActions

    def get_actions(self):
        return {'user_create': user_create}

    # IPackageController

    def _link_package_to_maintainer(self, context, pkg_dict):
        maintainer_email = pkg_dict.get('maintainer_email')
        if maintainer_email:
            session = context['session']
            user = session.query(model.User)\
                .filter(model.User.email.ilike(maintainer_email))\
                .first()
            if user:
                toolkit.get_action('package_collaborator_create')(context, {
                    'id': pkg_dict['id'],
                    'user_id': user.id,
                    'capacity': 'editor',
                })

    def after_create(self, context, pkg_dict):
        self._link_package_to_maintainer(context, pkg_dict)

