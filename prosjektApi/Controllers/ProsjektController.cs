﻿using Microsoft.AspNetCore.Mvc;
using ProsjektApi.Repos;
using prosjektApi.Utils;

namespace prosjektApi.Controllers
{

	public class ProsjektController : Controller
	{
		private Repo _repo;

		public ProsjektController(Repo repo)
		{
			_repo = repo;
		}

		[Route("/ckan/api/3/action/package_search")]
		public IActionResult Index(int rows, int start)
		{
			var projectsFromDb = _repo.GetProsjekter(rows, start);
			var projectsBudget = _repo.GetProjectBudgets();
			var projectsAsCKanResult = Mapper.MapToCKanResult(projectsFromDb, projectsBudget);
			return new JsonResult(projectsAsCKanResult);
		}

		[Route("/ckan/api/3/action/group_show")]
		public IActionResult Index(string id)
		{
			var departmentsFromdb = _repo.GetDepartment(id);
			var mapToGroupResult = Mapper.MapToGroupResult(departmentsFromdb);
			return new JsonResult(mapToGroupResult);
		}



	}
}
