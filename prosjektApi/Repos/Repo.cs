﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using prosjektApi.Domain;
using ProsjektApi.Domain;

namespace ProsjektApi.Repos
{
	public class Repo
	{
		private readonly ProsjektDbContext _context;

		public Repo(ProsjektDbContext context)
		{
			_context = context;
		}

		public IEnumerable<Project> GetProsjekter(int rows, int start)
		{

			return _context.Project.OrderBy(p => p.Prosjektnr).Skip(start).Take(rows);
		}

		

		public Department GetDepartment(string id)
		{
			return _context.Department.SingleOrDefault(d => d.Id == id);
		}

		public IEnumerable<ProjectBudget> GetProjectBudgets()
		{
			return _context.ProjectBudget.ToList();
		}
	}
}
