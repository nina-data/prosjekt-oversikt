﻿namespace prosjektApi.Models
{
	public class CKanGroupResultVm
	{
		public bool Success { get; set; }
		public GroupResultVm Result { get; set; }
	}
}