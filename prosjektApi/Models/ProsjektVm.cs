﻿using System;

namespace ProsjektApi.Models
{
	public class ProsjektVm
	{
		public string Prosjektnr { get; set; }
		public string Prosjektnavn { get; set; }
		public DateTime Startdato { get; set; }
		public DateTime Sluttdato { get; set; }
		public string Prosjektleder { get; set; }
		public string Avdeling { get; set; }
		public string Kategori { get; set; }
		public string Oppdragsgiver { get; set; }

	}
}
