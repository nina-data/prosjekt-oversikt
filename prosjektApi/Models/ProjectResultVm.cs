﻿using System.Collections.Generic;

namespace prosjektApi.Models
{
	public class ProjectResultVm
	{
		public string Id { get; set; }
		public string State { get; set; }
		public string Title { get; set; }
		public string Name { get; set; }
		public object Notes { get; set; }
		public bool Private { get; set; }
		public string Type { get; set; }
		public string Maintainer { get; set; }
		public string Project_state { get; set; }
		public string Maintainer_email { get; set; }
		public string Category { get; set; }
		public string Customer { get; set; }
		public string Startdate { get; set; }
		public decimal Budget { get; set; }
		public string Enddate { get; set; }
		public IEnumerable<GroupVm> Groups { get; set; }
	}

	public class GroupVm
	{
		public string Id { get; set; }
		public string Display_name { get; set; }
		public string Description { get; set; }
		public string Name { get; set; }
		public string Title { get; set; }
	}
}