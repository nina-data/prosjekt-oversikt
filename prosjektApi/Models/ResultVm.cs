﻿using System.Collections.Generic;
using prosjektApi.Controllers;

namespace prosjektApi.Models
{
	public class ResultVm
	{
		public int Count { get; set; }
		public string Sort { get; set; }
		public IEnumerable<ProjectResultVm> Results { get; set; }
	}
}