﻿namespace prosjektApi.Models
{
	public class GroupResultVm
	{
		public string Id { get; set; }
		public string Name { get; set; }
		public bool Is_organization { get; set; }
		public string Title { get; set; }
		public string Display_name { get; set; }
		public string Type { get; set; }

	}
}