﻿using System;
using System.Collections.Generic;
using System.Linq;
using prosjektApi.Domain;
using ProsjektApi.Domain;
using prosjektApi.Models;

namespace prosjektApi.Utils
{
	public static class Mapper
	{
		public static CKanResultVm MapToCKanResult(IEnumerable<Project> projectsFromDb, IEnumerable<ProjectBudget> projectBudgets)
		{

			var searchResult = new List<ProjectResultVm>();
			foreach (var project in projectsFromDb)
			{
				var groups = new List<GroupVm>();
				var group = new GroupVm
				{
					Id = project.AvdelingId,
					Description = project.Kategori,
					Name = project.AvdelingId,
					Display_name = project.Avdeling,
					Title = project.Avdeling
					
				};

				groups.Add(group);

				var proj = new ProjectResultVm
				{
					Id = project.Prosjektnr,
					Title = project.Prosjektnavn,
					Name = project.Prosjektnr,
					Maintainer = project.Prosjektleder,
					Project_state = GetStatusText(project.Status),
					Maintainer_email = project.Email,
					State = "active",
					Private = false,
					Type = "dataset",
					Category = project.Kategori,
					Customer = project.Oppdragsgiver,
					Startdate = project.Startdato.ToString("yyyy-MM-dd"),
					Enddate = project.Sluttdato.ToString("yyyy-MM-dd"),
					Groups = groups
				};

				var projectWithBudget = projectBudgets.SingleOrDefault(p => p.ProjectId == project.Prosjektnr);
				if (projectWithBudget != null)
				{
					proj.Budget = decimal.Round(projectWithBudget.Budget);
				}


				searchResult.Add(proj);
			}
			var resObj = new ResultVm
			{
				Count = searchResult.Count,
				Sort = "id asc",
				Results = searchResult
			};

			var res = new CKanResultVm
			{
				Success = true,
				Result = resObj
			};

			return res;
		}

		private static string GetStatusText(string projectStatus)
		{
			switch (projectStatus)
			{
				case "N":
					return "Aktiv";
					break;
				case "P":
					return "Parkert";
					break;
				case "D":
					return "Utkast";
					break;
				case "C":
					return "Sperret";
					break;
				case "T":
					return "Avsluttet";
					break;
				default:
					return "Ukjent";
					break;
			}
		}


		public static CKanGroupResultVm MapToGroupResult(Department department)
		{
			var result = new GroupResultVm
			{
				Id = department.Id,
				Name = department.Id,
				Display_name = department.Name,
				Title = department.Name,
				Is_organization = true,
				Type = "group"
			};

			var cKanGroupRes = new CKanGroupResultVm
			{
				Success = true,
				Result = result
			};

			return cKanGroupRes;

		}
		
	}
}