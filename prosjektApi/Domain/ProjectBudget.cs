﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace prosjektApi.Domain
{
	[Table("NINA_projects_budget")]
	public class ProjectBudget
	{
		[Key]
		[Column("Prosjekt")]
		public string ProjectId { get; set; }
		[Column("TotalRamme")]
		public decimal Budget { get; set; }

	}
}
