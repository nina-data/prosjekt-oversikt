﻿using Microsoft.EntityFrameworkCore;
using prosjektApi.Domain;

namespace ProsjektApi.Domain
{
	public class ProsjektDbContext : DbContext
	{
		public DbSet<Project> Project { get; set; }
		public DbSet<Department> Department { get; set; }
		public DbSet<ProjectBudget> ProjectBudget { get; set; }

		public ProsjektDbContext(DbContextOptions<ProsjektDbContext> options)
			: base(options)
		{
		}

	}

}
