﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using prosjektApi.Domain;

namespace ProsjektApi.Domain
{
	[Table("NINA_prosjekt_oversikt")]
	public class Project
	{
		[Key]
		public string Prosjektnr { get; set; }
		public string Prosjektnavn { get; set; }
		public DateTime Startdato { get; set; }
		public DateTime Sluttdato { get; set; }
		public string Prosjektleder { get; set; }
		public string Status { get; set; }
		[Column("e_mail")]
		public string Email { get; set; }
		public string Avdeling { get; set; }
		public string AvdelingId { get; set; }
		public string Kategori{ get; set; }
		public string Oppdragsgiver{ get; set; }
		
	}

	
}
