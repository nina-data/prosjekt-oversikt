﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace prosjektApi.Domain
{
	[Table("NINA_avdelinger")]
	public class Department
	{
		[Key]
		[Column("dim_value")]
		public string Id { get; set; } 
		[Column("description")]
		public string Name { get; set; }
	}
}
